﻿using System.Web;
using System.Web.Mvc;

namespace HW_02102020_API_JWT
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
