﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HW_02102020_API_JWT.Models
{
    public class UsuarioLogin
    {
        public string Usuario { get; set; }
        public string Password { get; set; }
    }
}